#!/bin/bash

#projectList=( EinsteinStyleLogicDrills/app )
projectList=()
list=$(find . -type f -name "*.lua");
pwd=$(pwd);
for i in $list
do
	filename=$(echo "$i" | grep -o -E '([-_[:alnum:]]+\.lua)')
	dir=$(echo "$i" | grep -o -E '[^/]+/[-_[:alnum:]]+\.lua')
	#echo "FILENAME:" $filename
	#echo "DIR:" $dir
	parentDir=$(echo "$dir" | cut -d / -f 1)
	
	if [ "$parentDir" == "." ]; then
		continue
	fi

	#echo "Parent:" $parentDir
	#echo "Filename:" $filename
	
	# For each project using these libs
	for project in $projectList
	do
		# Uncomment to get ALL file aliased in project directory
		#ln -s $pwd/$parentDir/$filename /Users/enfour/Documents/N4/projects/$project/$parentDir-$filename
		echo ""
	done
	mkdir -p $pwd/softlinks/$parentDir
	
	# Softlink
	ln -s $pwd/$parentDir/$filename $pwd/softlinks/$parentDir/$filename

	# Hardlink
	#ln $pwd/$parentDir/$filename $pwd/softlinks/$parentDir/$filename
done
