local Utils = require( 'lib-utils' )
local Input = require( 'lib-input' )

local MOAIProp2D = MOAIProp2D
local MOAITouchSensor = MOAITouchSensor

local print = print
local printf = Utils.printf
local tostring = tostring
local string = string
local assert = assert
local unpack = unpack
local pairs = pairs
local type = type

local MixinDragDrop = {}
setfenv( 1, MixinDragDrop )

--== Private functions
local function _enableTouch( self, aEnable )
	print( "dragdrop-_enableTouch", tostring( self._name ), tostring( aEnable ), tostring( self.isFocus ) )
	self.touchEnabled = aEnable
	if self.isFocus then
		-- Cancel tracking
assert( false, 'TODO' )
				Input.unlockTouch( aTouchNo )

		display.getCurrentStage():setFocus( self, nil )
		self.isFocus = false
	end
end

local function _onTouch( self, aTargetProp, aEventType, aTouchNo, aX, aY, aTapCount )
	--print( "dragdrop-_onTouch", tostring( aTargetProp.name ), aEventType, aX, aY )

	if self.touchEnabled then
	
		if aEventType == MOAITouchSensor.TOUCH_DOWN then
			
			-- Track a specific finger (aEvent.id)
			Input.lockTouchToProp( aTouchNo, aTargetProp )
			self.isFocus = true	

			-- Relative to initial grab point, rather than object "snapping"
			self.startX, self.startY = aTargetProp:getLoc()
			self.touchStartX, self.touchStartY = aX, aY

			-- Delegate call back 
			if self.onStartDrag ~= nil then
				self:onStartDrag( aTargetProp )
			end	
		elseif self.isFocus then
			if aEventType == MOAITouchSensor.TOUCH_MOVE and self.isDraggable then
				-- print( 'move', x, y, self.x0, self.y0 )
				local deltaX, deltaY = (aX - self.touchStartX), (aY - self.touchStartY)
				aTargetProp:setLoc( self.startX + deltaX, self.startY + deltaY )

				-- Delegate callback
				if self.onDragging ~= nil then
					self:onDragging( aTargetProp )
				end
			
			elseif aEventType == MOAITouchSensor.TOUCH_UP then
				-- Delegate callback
				if self.onEndDrag ~= nil then
					self:onEndDrag( aTargetProp )
				end
				
				-- Cancel tracking
				Input.unlockTouch( aTouchNo )
				self.isFocus = false
			end
		end
	end
end

local function _clean( self )
	self:removeEventListener( "touch", self )
end

local function _dragOn( self )
	self.isDraggable = true
end

local function _dragOff( self )
	self.isDraggable = false
end

--== Public

--[[
== Interface ========================
Optionally define the following on your object to handle events

@param aEvent { Event } 
onStartDrag( aTarget )

@param aEvent { Event } 
onDragging( aTarget )

@param aEvent { Event } 
onEndDrag( aTarget )
]]

function add( aObj, aTargetProp )
	assert( aObj and type( aObj ) == "table" )
	assert( aTargetProp and type( aTargetProp ) == "userdata" )
	assert( aTargetProp.name )

	aObj.onTouch = _onTouch
	aObj.enableTouch = _enableTouch
	aObj.clean = _clean
	aObj.touchEnabled = true
	aObj.isDraggable = true
	aObj.dragOn = _dragOn
	aObj.dragOff = _dragOff
	aObj.isFocus = false

	Input.setTouchListener( aTargetProp, aObj )
end

return MixinDragDrop