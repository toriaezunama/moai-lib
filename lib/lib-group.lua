local Utils = require( 'lib-utils')

local MOAIProp2D = MOAIProp2D
local print = print
local assert = assert
local string = string
local tostring = tostring
local pairs = pairs
local ipairs = ipairs
local type = type

local Group = {}
setfenv( 1, Group )

-- Added to props passed in to Group:addProp() as a convenience function for Group:remove( aProp )
local __removeSelf = function( self )
	local parent = self.parent
	if parent ~= nil and type( parent ) == 'userdata' then
		print( 'removeSelf' )
		parent:removeChild( self )
	end
end

-- Group functions
local metatable = {
	addChild = function( self, aProp )
		--print( "addChild", self.name, aProp.name )
		--==== If already in this group then return ====
		if self.children[ aProp ] then
			print( "Group: child already in this group!", tostring( self.name), tostring( self.name ))
			assert( false )
			return
		end

		--==== If already in a different group remove it first ====
		if aProp.parent then
			aProp.parent:removeChild( aProp )
		end

		--print( 'addChild' )
		assert( aProp.parent == nil, string.format( "Error: Group:addChild( %s ) - aProp already has a parent!", tostring( aProp ) ) )

		aProp.parent = self
		self.children[ aProp ] = aProp
		self.childCount = self.childCount + 1
		self.childrenArray[ self.childCount ] = aProp

		--== Link them together
				-- *** The following doesn't work **** prop:setNodeLink( parent )
				-- From: http://getmoai.com/forums/setnodelink-t331/
				-- setParent() is shorthand for: 
		--aProp:setAttrLink ( MOAIProp2D.INHERIT_COLOR, self, MOAIProp2D.COLOR_TRAIT ) 
		aProp:setAttrLink( MOAIProp2D.INHERIT_TRANSFORM, self, MOAIProp2D.TRANSFORM_TRAIT ) 
		
		-- TODO: we want children of this node to be able to override the group's setting
		--  BUT when you set the group's visibility it should override the children
		
		-- Setting this means the child props downstream will have their visibility over ridden by this parent group
		aProp:setAttrLink( MOAIProp2D.ATTR_VISIBLE, self, MOAIProp2D.ATTR_VISIBLE ) 
		--print( "linking", aProp:getAttrLink( MOAIProp2D.ATTR_VISIBLE ) )

		--== decorate aProp with a function to remove it from it's parent group
		aProp.removeSelf = __removeSelf
	end,

	-- aProp: 
	removeChild = function( self, aProp )
		--print( 'removeChild', aProp.name )
		self.children[ aProp ] = nil

		-- Find child in array
		local i
		for i, c in ipairs( self.childrenArray[ self.childCount ] ) do
			if c == aProp then
				break
			end
		end
		table.remove( self.childrenArray, i )

		self.childCount = self.childCount - 1

		--== Unlink 
		aProp:clearAttrLink( MOAIProp2D.INHERIT_TRANSFORM ) 
		aProp:clearAttrLink( MOAIProp2D.ATTR_VISIBLE ) 

		aProp.parent = nil
	end,

	childCount = function( self )
		return self.childCount
	end,

	--== Breadth first ==
	forEachDescendantBreadth = function( self, aCallback )
		--print( 'forEachDescendant' )
		local queue = {}
		Utils.extendTable( queue ) -- add pop, push etc

		queue:push( self )

		local prop = queue:popFront()
		while prop do
			--print( prop.name, prop.childCount )
			if prop.childCount and prop.childCount > 0 then
				for _, child in ipairs( prop.childrenArray ) do
					queue:pushBack( child )
				end
			end
			local doBreak = aCallback( prop )
			if doBreak then
				break
			end 

			--print( "Q length", #queue )
			prop = queue:popFront()
		end
	end,

	forEachDescendantDepth = function( self, aCallback )
		function recurse( aNode )
			if not aNode then
				return false
			end

			local doBreak = aCallback( aNode )
			if doBreak then
				print( "Breaking")
				return true
			end 

			if aNode.children then
				for _, child in ipairs( aNode.childrenArray ) do
					if recurse( child ) then
						return true
					end
				end 
			end
		end

		recurse( self )
	end,

	findByName = function( self, aName )
		local found = nil
		self:forEachDescendantBreadth( function( aNode ) 
			if aNode.name and aNode.name == aName then
				found = aNode
				return true -- cause a break
			end
		end )
		return found
	end,

	-- set/get visiblity
	isVisible = function( self, aIsVisible )
		if aIsVisible ~= nil then
			self:setVisible( aIsVisible )
			self._isVisible = aIsVisible
		end
		return self._isVisible
	end
}


function Group.new( aProp )
	local group
	-- In the case of an existing prop just decorate with group functions
	if aProp ~= nil then
		group = aProp

		-- Guard against calling new() on an existing Group
		if group.isGroup then
			return
		end
	else
		-- Native MOAI objects are of type 'userdata' and hence you cannot use setmetatable() on them
		group = MOAIProp2D.new() -- using MOAIProp2D NOT MOAITransform2D so that we can inherit visibility, color etc
	end

	group.isGroup = true

	-- therefore we decorate the MOAI object with member functions
	for funcName, func in pairs( metatable ) do
		group[ funcName ] = func
	end	

	group.children = {}  -- for quick checks to see if we contain a certain child prop
	group.childrenArray = {}	-- required so that traversing children is done in the order they are added
	group.childCount = 0
	group._isVisible = true

	return group
end

return Group
