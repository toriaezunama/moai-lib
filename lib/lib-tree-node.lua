local Utils = require( 'lib-utils')

local print = print
local assert = assert
local string = string
local tostring = tostring
local pairs = pairs
local ipairs = ipairs
local type = type
local getmetatable = getmetatable
local setmetatable = setmetatable

local TreeNode = {}
setfenv( 1, TreeNode )

--==== TreeNode member functions ====
local TreeNode_mt = {}

function TreeNode_mt:addChild( aNode )
	assert( aNode.isTreeNode )

	--==== If already in a different Tree then remove it first ====
	if aNode.parentTreeNode then
		aNode.parentTreeNode:removeChild( aNode )
	end

	--==== Check that node isn't already a child  ====	
	if self:isChild( aNode ) then
		return
	end

	--==== Add ====
	aNode.parentTreeNode = self
	self.children[ #self.children + 1 ] = aNode
end

function TreeNode_mt:removeSelf()
	local parent = self.parentTreeNode
	if parent ~= nil and parent.isTreeNode then
		parent:removeChild( self )
	end
end

function TreeNode_mt:getParent()
	return self.parentTreeNode
end

function TreeNode_mt:getIndexForChild( aNode )
	--==== Check child not already in our list ====	
	for i, c in ipairs( self.children ) do
		if c == aNode then
			return i 
		end
	end	
	return nil
end

function TreeNode_mt:isChild( aNode )
	if self:getIndexForChild( aNode ) ~= nil then
		return true
	else
		return false
	end
end
	
function TreeNode_mt:removeChild( aNode )
	local i = self:getIndexForChild( aNode )
	if i then
		table.remove( self.childrenArray, i )
	end
	aNode.parentTreeNode = nil
end

function TreeNode_mt:childCount()
	return #self.children
end

--== Breadth first ==
function TreeNode_mt:forEachDescendantBreadth( aCallback )
	--print( 'forEachDescendant' )
	local queue = {}
	Utils.extendTable( queue ) -- add pop, push etc

	queue:push( self )

	local node = queue:popFront()
	while node do
		--print( prop.name, prop.childCount )
		for _, child in ipairs( node.children ) do
			queue:pushBack( child )
		end

		local doBreak = aCallback( node )
		if doBreak then
			break
		end 

		--print( "Q length", #queue )
		node = queue:popFront()
	end
end

function TreeNode_mt:forEachDescendantDepth( aCallback )
	function recurse( aNode )
		if not aNode then
			return false
		end

		local doBreak = aCallback( aNode )
		if doBreak then
			-- print( "Breaking")
			return true
		end 

		for _, child in ipairs( aNode.children ) do
			if recurse( child ) then
				return true
			end
		end 
	end

	recurse( self )
end

function TreeNode_mt:findByName( aName )
	local found = nil
	self:forEachDescendantBreadth( function( aNode ) 
		if aNode.name and aNode.name == aName then
			found = aNode
			return true -- cause a break
		end
	end )
	return found
end

--==== PUBLIC ====
function TreeNode.new( aTreeNode )
	if aTreeNode then
		local t = type( aTreeNode )
		assert( t == 'table' or t == 'userdata' )

		--==== Already a TreeNode ====
		if aTreeNode.isTreeNode then 
			return
		end

		local mt = getmetatable( aTreeNode )
		--==== Use proxy userdata or existing metatable ====
		if t == 'userdata' then
			assert( false, 'TODO' )
			proxy = {}
			setmetatable( proxy, { __index = TreeNode_mt } )
			setmetatable( aTreeNode, { __index = proxy } )		

		--==== table with an existing metatable ====
		elseif mt then
			assert( false, 'TODO' )

		--==== plain table ====
		else
			setmetatable( aTreeNode, { __index = TreeNode_mt } )
		end 

	--==== No param then create a new TreeNode ====
	else
		aTreeNode = {}
		setmetatable( aTreeNode, { __index = TreeNode_mt } )
	end

	aTreeNode.isTreeNode = true
	aTreeNode.children = {}

	--==== Allow chaining ====
	return aTreeNode 
end

return TreeNode
