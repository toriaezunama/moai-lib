
local Init = require( 'lib-init' )

local Utils = require( 'lib-utils' )
local TextureManager = require 'lib-texture-manager'

local MOAITimer = MOAITimer
local MOAIProp2D = MOAIProp2D

local math = math
local printf = Utils.printf
local print_r = Utils.print_r
local print = print
local string = string
local tostring = tostring
local assert = assert
local unpack = unpack
local table = table
local ipairs = ipairs
local pairs = pairs
local require = require
local setmetatable = setmetatable

local Sprite = {}
setfenv( 1, Sprite )

local mt_sprite = {}

function mt_sprite:addAnim( aName, aFrameList, aFPS, aLoop )
	-- print( 'mt_sprite:addAnim()', aName, aFrameList, aFPS, aLoop )
	-- print_r( aFrameList )
	if aLoop == nil then aLoop = true end  -- default: loop

	--==== Convert names to deck indices ====
	local frames = {}
	local deck = self.prop.deck
	for i, f in ipairs( aFrameList ) do
		frames[ i ] = deck:indexForName( f )
	end

	local newAnim = {
		currentFrameIdx = 1,
		frames = frames,
		fps = aFPS,
		loop = aLoop
	}

	self.anims[ aName ] = newAnim

	self:_setAnim( aName )
end

function mt_sprite:_update()
	local animData = assert( self.currentAnimData )
	self.prop:setIndex( animData.frames[ animData.currentFrameIdx ] )
	-- self.prop:setRefPoint( self.refPoint )
end

local function _timerCallback( aTimer )
	--print( '_timerCallback()', aTimer )
	local sprite = aTimer.owner
	local times = aTimer:getTimesExecuted()

	local animData = sprite.currentAnimData
	local frameIdx = animData.currentFrameIdx + 1
	if frameIdx > #animData.frames then
		if not animData.loop then
			aTimer:stop()
			frameIdx = frameIdx - 1 -- set back to last frame
		else
			frameIdx = 1 -- loop
		end
	end
	animData.currentFrameIdx = frameIdx
	sprite:_update()
end

function mt_sprite:_setAnim( aName )
	self.currentAnim = aName
	self.currentAnimData = self.anims[ aName ]	
end

function mt_sprite:frame( aFrameNo )
	local animData = self.currentAnimData
	-- Clamp value to valid range for the current anim
	aFrameNo = math.min( math.max( 1, aFrameNo ), #animData.frames )
	animData.currentFrameIdx = aFrameNo

	self.timer:stop()

	self:_update()
end

function mt_sprite:play( aName, aReset )
	-- print( 'sprite:play()', aName, tostring( aReset ))
	assert( aName )
	aReset = aReset or false
	-- printf( "play( %s, reset: %s )", aName, tostring( aReset ) )

	--==== Already playing ====
	if self.isPlaying and self.currentAnim == aName then
		if aReset then -- reset frame if necessary
			local data = self.currentAnimData
			data.currentFrameIdx = 1
			self:_update()
		end
	else
		self.isPlaying = true

		self:_setAnim( aName )

		local animData = self.currentAnimData
		self:_update()

		local timer = self.timer
		timer:stop()
		timer:setSpan( animData.fps ) -- 12 fps
		timer:start()
	end
end

function mt_sprite:stop()
	self.timer:stop()
	sprite.isPlaying = false
end

local LEFT = 1
local RIGHT = 2
function mt_sprite:faceLeft()
	-- print( 'faceLeft()' )
	local _, y = self.prop:getScl()
	self.prop:setScl( -1, y )
end

function mt_sprite:faceRight()
	-- print( 'faceRighta()' )
	local _, y = self.prop:getScl()	
	self.prop:setScl( 1, y )
end

function Sprite.new( aTexture, aLayer ) 
	assert( aTexture )

	local sprite = {}
	setmetatable( sprite, { __index = mt_sprite } )

	sprite.anims = {}
	sprite.texture = aTexture

	local prop = TextureManager:newPackedImage( nil, aTexture )
	sprite.prop = prop
	-- prop:flipY()
	sprite.refPoint = MOAIProp2D.REF_PNT_C 
	-- prop:setRefPoint( sprite.refPoint )

	local timer = MOAITimer.new()
	sprite.timer = timer
	timer:setMode( MOAITimer.LOOP )   -- MOAITimer.[NORMAL|REVERSE|LOOP|LOOP_REVERSE|PING_PONG]
	timer:stop()
	timer.owner = sprite -- reference back to us
	timer:setListener( MOAITimer.EVENT_TIMER_END_SPAN, _timerCallback )

	sprite.isPlaying = false

	if aLayer then
		aLayer:insertProp( prop )
	end

	return sprite
end

function mt_sprite:clone()
	local sprite = {}
	setmetatable( sprite, { __index = mt_sprite } )

	sprite.anims = {}

	local prop = TextureManager:newPackedImage( nil, self.texture )
	sprite.prop = prop
	prop:flipY()
	sprite.refPoint = self.refPoint
	-- prop:setRefPoint( sprite.refPoint )

	local timer = MOAITimer.new()
	sprite.timer = timer
	timer:setMode( MOAITimer.LOOP )   -- MOAITimer.[NORMAL|REVERSE|LOOP|LOOP_REVERSE|PING_PONG]
	timer:stop()
	timer.owner = sprite -- reference back to us
	timer:setListener( MOAITimer.EVENT_TIMER_END_SPAN, _timerCallback )

	sprite.isPlaying = false

	--==== Copy anims ====
	sprite.anims = {}
	for name, anim in pairs( self.anims ) do
		sprite.anims[ name ] = Utils.copyTable( anim )
	end

	sprite.currentAnim = self.currentAnim
	sprite.currentAnimData = sprite.anims[ self.currentAnim ]	

	return sprite
end


return Sprite