--[[ 
== Example use ==
...
]]

--== Built in functions 
local assert = assert
local error = error
local pairs = pairs
local ipairs = ipairs
local print = print
local setmetatable = setmetatable
local tonumber = tonumber
local tostring = tostring
local type = type
local unpack = unpack
local table = table
local string = string

local Utils = require "lib-utils"
local MOAIHttpTask = MOAIHttpTask

--== Define what we want to be available in the sandbox
local Instapaper = {}
setfenv( 1, Instapaper )

local URL_AUTHENTICATE = 'https://www.instapaper.com/api/authenticate'
local URL_BOOKMARK = 'https://www.instapaper.com/api/add'

local validateDefaults = {
	username = "", 
	password = "",	
	isPost = true, -- true = POST| false = GET
	isVerbose = false,
	callback = function( aResponseCode )
		local msg = 'instapaper:validateUser: '
		if responseCode == "200" then
			msg = msg .. 'OK'
		elseif aResponseCode == "403" then
			msg = msg .. 'Invalid username or password.'
		elseif responseCode == "500" then
			msg = msg .. 'The service encountered an error. Please try again later.'
		else
			msg = msg .. 'Unknown error(' .. tostring( aResponseCode ) .. ')'
		end 
		print( msg )
	end
}

function validateUser( aParams )
	local aParams = aParams or {}
	local params = Utils.newTableFromDefaultsOverriddenWithParams( validateDefaults, aParams );
	local task = MOAIHttpTask.new()

	if params.isPost then
		task:setVerb( MOAIHttpTask.HTTP_POST )
	else
		task:setVerb( MOAIHttpTask.HTTP_GET )
	end

	local url = string.format( "%s?username=%s&password=%s", URL_AUTHENTICATE, Utils.urlEncode( params.username ), 
						params.password )
	--print( url )
	task:setUrl( url )
	task:setCallback( function( aTask, aResponseCode )
		-- if( task:getSize () ) then
		-- 	print( task:getString () )
		-- end
		params.callback( aResponseCode )
	end )
	task:setUserAgent ( "Moai" )
	-- task:setHeader ( "Foo", "foo" )
	-- task:setHeader ( "Bar", "bar" )
	-- task:setHeader ( "Baz", "baz" )
	if params.isVerbose then
		task:setVerbose ( true )
	end
	task:performAsync()
end

local bookmarkDefaults = {
	username = "", 
	password = "",	
	isPost = true, -- true = POST| false = GET
	isVerbose = false,
	callback = function( aResponseCode )
		local msg = 'instapaper:validateUser: '
		if responseCode == "200" then
			msg = msg .. 'OK'
		elseif aResponseCode == "400" then
			msg = msg .. 'Bad request or exceeded the rate limit. Probably missing a required parameter, such as url.'
		elseif aResponseCode == "403" then
			msg = msg .. 'Invalid username or password.'
		elseif responseCode == "500" then
			msg = msg .. 'The service encountered an error. Please try again later.'
		else
			msg = msg .. 'Unknown error(' .. tostring( aResponseCode ) .. ')'
		end 
		print( msg )
	end
}


function bookmark( aUrl )
	--[[
	username and password (Or you can pass the username and password via HTTP Basic Auth.)
	url
	title — optional, plain text, no HTML, UTF-8. If omitted or empty, Instapaper will crawl the URL to detect a title.
	selection — optional, plain text, no HTML, UTF-8. Will show up as the description under an item in the interface. Some clients use this to describe where it came from, such as the text of the source Twitter post when sending a link from a Twitter client.
	redirect=close — optional. Specifies that, instead of returning the status code, the resulting page should show an HTML “Saved!” notification that attempts to close its own window with Javascript after a short delay. This is useful if you’re sending people directly to /api/add URLs from a web application.
	jsonp — optional. See JSONP.
	Resulting status codes:

	If a URL is added that already exists, it will be marked unread and sent to the top of the list. 
	A duplicate will not be created. For convenience, the 201 status will still be returned, even though a 
	new record technically wasn’t created.

	When a 201 is returned from this call, two additional response headers are set:

	Content-Location: The saved URL for the newly created item, after any normalization and redirect-following.
	X-Instapaper-Title: The saved title for the page, after any auto-detection.
	]]
end

return Instapaper

--[[
username and password (Or you can pass the username and password via HTTP Basic Auth.)
url
title — optional, plain text, no HTML, UTF-8. If omitted or empty, Instapaper will crawl the URL to detect a title.
selection — optional, plain text, no HTML, UTF-8. Will show up as the description under an item in the interface. Some clients use this to describe where it came from, such as the text of the source Twitter post when sending a link from a Twitter client.
redirect=close — optional. Specifies that, instead of returning the status code, the resulting page should show an HTML “Saved!” notification that attempts to close its own window with Javascript after a short delay. This is useful if you’re sending people directly to /api/add URLs from a web application.]]