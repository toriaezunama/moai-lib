--[[
-- === USAGE ===

local observer = {}
function observer:onTouch( aEventType, aTouchNo, aX, aY, aTapCount )
	print( 'touch',  aEventType, aTouchNo, aX, aY, aTapCount )
end

function observer:onMouseMove( self, aX, aY ) 
	print( 'onMouseMove', aX, aY )
end

function observer:onMouseClick( aIsLeftButton, aIsDown )
	print( 'onMouseClick',  aIsLeftButton, aIsDown  )
end

function observer:onKey( aKey, aIsDown )
	print( 'onKey', aKey, aIsDown )
end

Input.setMouseListener( observer )
Input.setTouchListener( observer )
Input.setKeyboardListener( observer )

]]
local Utils = require( 'lib-utils' )

local MOAIInputMgr = MOAIInputMgr
local MOAITouchSensor = MOAITouchSensor
local math = math
local printf = Utils.printf
local print_r = Utils.print_r
local print = print
local string = string
local tostring = tostring
local assert = assert
local unpack = unpack
local table = table
local ipairs = ipairs
local pairs = pairs
local require = require
local setmetatable = setmetatable
local MOAIProp2D = MOAIProp2D

local _mouse = MOAIInputMgr.device.pointer
local _keyboard = MOAIInputMgr.device.keyboard
local _mouseLeft = MOAIInputMgr.device.mouseLeft
local _mouseRight = MOAIInputMgr.device.mouseRight
local _touch = MOAIInputMgr.device.touch
local _mouseX, _mouseY = 0, 0
local _isTouchActive = false
local _activeTouchCnt = 0

local Input = {}
setfenv( 1, Input )


--==== Keyboard keys ====
Input.KEY_UP_ARROW = 357
Input.KEY_DOWN_ARROW = 359
Input.KEY_LEFT_ARROW = 356
Input.KEY_RIGHT_ARROW = 358
Input.KEY_SPACE = 32

Input.KEY_W = 119
Input.KEY_A = 97
Input.KEY_S = 115
Input.KEY_D = 100
Input.KEY_R = 114

local _lockedTouches = {}
local _touchLayerStack = {}
Utils.extendTable( _touchLayerStack )

--==== Observer lists  ====
local _mouseObserverList = {}
local _touchObserverList = {}
local _keyboardObserverList = {}

--==== Local event handlers ====
local function _onKeyboardEvent( aKey, aIsDown )
	for _, observer in ipairs( _keyboardObserverList ) do
		if observer.onKey then 
			observer:onKey( aKey, aIsDown )
		end
	end
end

local function _pickProp( aX, aY )
	local topProp
	local x, y, layer
	--==== Start with top layer and work down until we find a pick ====
	for i = #_touchLayerStack, 1, -1 do
		layer = _touchLayerStack[ i ]
		x, y = layer:wndToWorld( aX, aY )
		local partition = layer:getPartition()
		topProp = partition:propForPoint( x, y )
		if topProp then
			break
		end
	end
	return topProp, x, y, layer
end

local function _callOnTouchHandlers( aTargetProp, aEventType, aTouchNo, aX, aY, aTapCount )
	--==== Bubbling ====
	local prop = aTargetProp
	while prop ~= nil do
		local list = _touchObserverList[ prop ]
		if list then
			for i, handlerObj in ipairs( list ) do
				if handlerObj.onTouch then
					handlerObj:onTouch( aTargetProp, aEventType, aTouchNo, aX, aY, aTapCount )
				end
			end
		end

		prop, _ = prop:getAttrLink( MOAIProp2D.INHERIT_TRANSFORM )
	end
	--==== TODO: Implement global touch handler as a final stop for touch handling ====

end

local function _onTouchEvent( aEventType, aTouchNo, aX, aY, aTapCount )
	--print( "TOUCH", aEventType, aTouchNo, aX, aY, aTapCount )
	if aEventType == MOAITouchSensor.TOUCH_DOWN then
	elseif aEventType == MOAITouchSensor.TOUCH_MOVE then
	end	

	--==== Locked touches get touch events even when they aren't being touched i.e. picking is skipped ====
	local prop = _lockedTouches[ aTouchNo ]
	local worldX, worldY, layer
	if not prop then 
		prop, worldX, worldY, layer  = _pickProp( aX, aY )
		-- print( "picked", tostring( prop ), prop.name )
	else
		-- print( 'locked' )
		worldX, worldY = prop.layer:wndToWorld( aX, aY )
	end
	if prop then
		if layer then 
			prop.layer = layer
		end
		--print( 'here' )
		_callOnTouchHandlers( prop, aEventType, aTouchNo, worldX, worldY, aTapCount )
	end

	if aEventType == MOAITouchSensor.TOUCH_UP then
		--==== Automatically remove touch because touchNo will be recycled in a future touch ====
		unlockTouch( aTouchNo )	
	end

	--==== Multitouch ====
    --print( aTouchNo, x, y, tapCount )
	-- local s = ''
	-- if touchSensor:hasTouches() then
	-- 	s = '-----------Touch event-------------\n'
	-- 	s = s .. string.format( '-- Touch %s --\n', aEventType )
	-- 	-- Get id's of touches
	-- 	local t1, t2, t3, t4, t5, t6, t7, t8, t9, t10 = touchSensor:getActiveTouches()
	-- 	local touchIdList = { t1, t2, t3, t4, t5, t6, t7, t8, t9, t10 }
	-- 	local x, y, tapCount
	-- 	for _, touchId in ipairs( touchIdList ) do
	-- 		if touchId ~= nil then
	-- 			x, y, tapCount = touchSensor:getTouch( touchId )
	-- 			s = s .. string.format( "[%d] (%d,%d), %d taps\n", touchId, x, y, tapCount )
	-- 		end
	-- 	end
	-- end
end

--==== These 3 share the same observer list (_mouseObserverList)  ====
local function _onMouseEvent( aXScreen, aYScreen )
	_mouseX, _mouseY = aXScreen, aYScreen

	--==== Simulate a touch ====
	if _isTouchActive then
		_onTouchEvent( MOAITouchSensor.TOUCH_MOVE, 1, _mouseX, _mouseY, 1 )	
	end

	--==== Callbacks ====
	-- for _, observer in ipairs( _mouseObserverList ) do
	-- 	if observer.onMouseMove then
	-- 		observer:onMouseMove( mouseX, mouseY )
	-- 	end
	-- end
end

local function _onLeftClickEvent( aIsDown )
	--==== Simulate a single touch ====
	if aIsDown then
		_isTouchActive = true
		_onTouchEvent( MOAITouchSensor.TOUCH_DOWN, 1, _mouseX, _mouseY, 1 )	
	else
		_isTouchActive = false
		_onTouchEvent( MOAITouchSensor.TOUCH_UP, 1, _mouseX, _mouseY, 1 )	
	end
	-- for _, observer in ipairs( _mouseObserverList ) do
	-- 	if observer.onMouseClick then
	-- 		observer:onMouseClick( "left", aIsDown )
	-- 	end
	-- end
end

local function _onRightClickEvent( aIsDown )
	-- for _, observer in ipairs( _mouseObserverList ) do
	-- 	if observer.onMouseClick then
	-- 		observer:onMouseClick( "right", aIsDown )
	-- 	end
	-- end
end


--==== Set MOAI input handlers ====
if _keyboard then
	_keyboard:setCallback( _onKeyboardEvent )
end

if _mouse then
	_mouse:setCallback( _onMouseEvent )
end

if _mouseLeft then
	_mouseLeft:setCallback( _onLeftClickEvent )
end

if _mouseRight then
	_mouseRight:setCallback( _onRightClickEvent )
end

if _touch then
	_touch:setCallback( _onTouchEvent ) -- Called when anything happens for a touch on screen
end

--==== Make into **Weak tables** so that table entries that reference deleted objects are removed automatically ====
setmetatable( _mouseObserverList, { __mode = 'v' } )
setmetatable( _touchObserverList, { __mode = 'v' } )
setmetatable( _keyboardObserverList, { __mode = 'v' } )

--==== PUBLIC ====
function setMouseListener( aObj )
	_mouseObserverList[ #_mouseObserverList + 1 ] = aObj
end

function setTouchListener( aTargetProp, aListenerObj )
	assert( aTargetProp )
	--==== Default to aTargetProp as the listener when not specified ====
	aListenerObj = aListenerObj or aTargetProp

	local list = _touchObserverList[ aTargetProp ]
	if not list then 
		list = {}
		Utils.extendTable( list )
		_touchObserverList[ aTargetProp ] = list
	end
	if not list:isValueInTable( aListenerObj ) then
		list:push( aListenerObj )
	end
end

function setKeyboardListener( aObj )
	_keyboardObserverList[ #_keyboardObserverList + 1 ] = aObj
end

local function _getLayerIdx( aLayer )
	for i, l in ipairs( _touchLayerStack ) do
		if l == aLayer then
			return i
		end
	end
	return nil
end

--==== _touchLayerStack is a stack so LAST element is on TOP ====
function pushLayerForPicking( aLayer )
	addLayerForPickingAtIdx( aLayer, #_touchLayerStack )
end

function addLayerForPickingAtIdx( aLayer, aIdx )
	aIdx = Utils.clamp( aIdx, 1, #_touchLayerStack )

	--==== Is aLayer already in our list ====
	local layerIdx = _getLayerIdx( aLayer )
	if layerIdx then
		table.remove( _touchLayerStack, layerIdx )
	end

	table.insert( _touchLayerStack, aIdx, aLayer )

	--Utils.print_r( _touchLayerStack )
end

function lockTouchToProp( aTouchNo, aProp )
	Utils.printf( "lockTouchToProp( %d, %s ), %d", aTouchNo, aProp.name, _activeTouchCnt )
	-- assertsert( aTouchNo >= 0 and aTouchNo <= _activeTouchCnt )
	_lockedTouches[ aTouchNo ] = aProp
end

function unlockTouch( aTouchNo )
	-- Utils.printf( "unlockTouch( %d ), %d", aTouchNo, _activeTouchCnt )
	--assert( aTouchNo >= 0 and aTouchNo <= _activeTouchCnt )
	_lockedTouches[ aTouchNo ] = nil
end

return Input