module( ..., package.seeall )


--[[=============================================
TODO:
- save/loadTableToFile - reimplement using moai's serialize/deserialize funcs
- Function to print globals that have been defined. 
  To be used to check for accidental definition of globals.
  Should filter out globals already defined as part of the environment

===============================================]]

--[[
FUNCTION LIST (## not in use)
HexRGBToDec( aRGB )
setWithDefault( aOrig, aDefault, aNew )	
newTableFromDefaultsOverriddenWithParams( aDefaults, aParams )
##pointInBounds( aX, aY, aBounds )
##_fitMultilineTextIntoCell( aLines, aCellH, aFontSize )
##fileCopy( srcName, srcPath, dstName, dstPath )	
copyTable( aSrcTable, aDestTable, aOverwrite )
tableKeyCount( aTable )
tableKeys( aTable )
saveTableToFile( aTable, aFilePath, aPretty )
loadTableFromFile( aFilePath )	
urlEncode( aURL )
urlDecode( aURL )
trim(s)
split( str, pat, aTrim )
splitOnTags( aString )
shuffleArray( aArray )
concatArray( aSrc, aDst ) 
removeDuplicatesArray( aArray )
dump( aObj ) 
print_r ( t ) 
]]

--==== Forward references ====
printf = nil
print_r = nil

--===============================================
-- Memory
--===============================================
function getMemory( aPrecise )
	if aPrecise then 
		collectgarbage("collect")
	end
	return gcinfo() -- in kb
end

function printMemory( aPrecise )
	printf( "memory: %dk", getMemory( aPrecise ) )
end

--===============================================
-- Colour tools
--===============================================
function RGBAToReal( aR, aG, aB, aA )
	-- TODO: check this is correct
	local inv256 = 0.00390625 -- 1 / 256

	local res = {
		aR * inv256,
		aG * inv256,
		aB * inv256,
	}

	if aA ~= nil then
		res[ #res + 1 ] = aA * inv256
	end	
	return res
end

local MOAIColorExts = {
	getColor = function( self )
		return self:getAttr( MOAIColor.ATTR_R_COL ),
			self:getAttr( MOAIColor.ATTR_G_COL ),
			self:getAttr( MOAIColor.ATTR_B_COL ),
			self:getAttr( MOAIColor.ATTR_A_COL )
	end
}

function extendMOAIColor( aMOAIColor )
	aMOAIColor = aMOAIColor or MOAIColor.new()
	
	for fName, f in pairs( MOAIColorExts ) do
		aMOAIColor[ fName ] = f
	end

	return aMOAIColor
end

--===============================================
-- Timer
--===============================================
function performWithDelay( aDelayInSecs, aRepeatCnt, aCallback, ... )
	assert( type( aDelayInSecs ) == 'number' )

	--==== FIX: aDelayInSecs < 0.01 and the default sim step becomes larger than aDelayInSecs
	--==== causing it to loop multiple times per step and hence multiple callbacks will occur ====
	aDelayInSecs = clamp( aDelayInSecs, 0.01, 9999 )

	--==== You can skip aRepeatCnt as a parameter ====
	-- ( aDelayInSecs, aRepeatCnt, 	aCallback, ... )
	--         |            |		  |
	-- ( aDelayInSecs, aCallback, 	...            )
	if type( aRepeatCnt ) == 'function' then
		table.insert( arg, 1, aCallback ) -- arg[ 1 ] (aCallback is arg1)
		aCallback = aRepeatCnt
		aRepeatCnt = 1
	else
		-- Ensure no -ve's (except -1 for infinite loop)
		assert( type( aRepeatCnt) == 'number')
		if aRepeatCnt == 0 or aRepeatCnt < -1 then
			aRepeatCnt = 1
		end
	end

	--==== Create a timer ====
	local timer = MOAITimer.new()
	timer:setMode( MOAITimer.LOOP )   -- MOAITimer.[NORMAL|REVERSE|LOOP|LOOP_REVERSE|PING_PONG]
	timer:setSpan( aDelayInSecs )

	-- print( 'performWithDelay', aRepeatCnt, aDelayInSecs, timer )
	--print_r( arg )
	--print( debug.traceback() )

	--==== Listen for events on the timer ====
					-- MOAITimer.[EVENT_TIMER_KEYFRAME|EVENT_TIMER_LOOP|EVENT_TIMER_BEGIN_SPAN|EVENT_TIMER_END_SPAN]
	timer:setListener( MOAITimer.EVENT_TIMER_LOOP,  
		function( aTimer )
			-- print( 'timer', aTimer:getTimesExecuted(), aRepeatCnt, aTimer )
			if aRepeatCnt ~= -1 and aTimer:getTimesExecuted() >= aRepeatCnt then
				aTimer:stop()
				aTimer = nil  -- destroy timer object
			end
			aCallback( unpack( arg ) )
		end
	)
	timer:start()
	return timer
end

--===============================================
-- General
--===============================================
function nullFunc() 
end

function makeTableReadOnly( aTable )
	setmetatable( aTable, {
		__index = function( table, key )
			local v = rawget( table, key )
			if v ~= nil then 
				return v
			else
				error( "ERROR: ** Attempt to read undefined key: " .. tostring(key) .. ' **', 2 )		
			end
		end,
		__newindex = function( table, key, value )
			error( "ERROR: ** Attempt to update a read-only table **", 2 )
		end
	} )

	return aTable -- chaining
end

function unloadModule( aModule )
    package.loaded[ aModule ] = nil
    _G[ aModule ] = nil
end

-- if aNew is defined then aOrig = aNew else aOrig = aDefault
function setWithDefault( aOrig, aDefault, aNew )	
	assert( aDefault ~= nil )
	
	if aNew ~= nil then
		return aNew
	elseif aOrig == nil then
		return aDefault
	else
		return aOrig
	end
end
 
-- scale a source rect to fit a dest rect maintaining src's aspect ratio
-- a[Src|Dst]: { width, height }
function rectFit( aSrc, aDst )
	rectFit( aSrc.width, aSrc.height, aDst.width, aDst.height )
end

function rectFit( aSrcWidth, aSrcHeight, aDstWidth, aDstHeight, aFurthest )

	-- Scale either to match the closest dimension (default) or the furthest
	aFurthest = aFurthest or false
	local func = math.min
	if aFurthest then
		func = math.max
	end
	
	-- Find which dimension in the dst rect has the biggest difference to the src rect
	local scale = func( aDstWidth/aSrcWidth, aDstHeight/aSrcHeight)

	-- Scale on this
	return aSrcWidth * scale, aSrcHeight * scale
end
--[[
function pointInBounds( aX, aY, aBounds )
	if aX < aBounds.xMin or aX > aBounds.xMax or aY < aBounds.yMin or aY > aBounds.yMax then
		return false
	else
		return true
	end
end
]]
--[[
function _fitMultilineTextIntoCell( aLines, aCellH, aFontSize )
	-- gap at top (offset to first line)		
	local gap = ((aCellH - ( #aLines * aFontSize) ) * 0.5) 
	--print ( #aLines, aCellH, aFontSize, gap )
	local y = gap
	local res = {}
	for _, _ in ipairs( aLines ) do
		res[ #res + 1 ] = y
		y = y + aFontSize
	end
	
	return res
end
]]

function clamp( aValue, aLower, aUpper )
	return math.max( aLower, math.min( aValue, aUpper ) )
end
	
--===============================================
-- File functions
--===============================================
--[[
function fileCopy( srcName, srcPath, dstName, dstPath )	
	local res = true 
	
	-- Copy the source file to the destination file
	local rfilePath = system.pathForFile( srcName, srcPath )
	local wfilePath = system.pathForFile( dstName, dstPath )
	
	local rfh = io.open( rfilePath, "rb" )              
	local wfh = io.open( wfilePath, "wb" )
	
	if not wfh then
		print( "writeFileName open error!" )
		res = false                 -- error
	else
		-- Read the file from the source directory and write it to the destination directory
		local data = rfh:read( "*a" )
		
		if not data then
			print( "read error!" )
			res = false     -- error
		else
			if not wfh:write( data ) then
				print( "write error!" ) 
				res = false -- error
			end
		end
	end
	
	-- Clean up our file handles
	rfh:close()
	wfh:close()
	
	return res  
end
]]

--===============================================
-- Table functions
--===============================================
function pack( ... )
	return arg
end

--==== Copy src to dest overwriting values if aOverwrite is true ====
--==== N.B. If a key doesn't exist in dest then it is created! ====
local function _copyTable( aSrcTable, aDestTable, aOverwrite )
	for k,v in pairs( aSrcTable ) do
		if type( v ) == 'table' then
			--==== Create table if it doesn't exist ====
			if aDestTable[ k ] == nil then
				aDestTable[ k ] = {}
			end
			--==== Recurse ====
			_copyTable( v, aDestTable[ k ], aOverwrite )
		else
			--==== Conditionally copy across values ====
			if aDestTable[ k ] == nil then
				aDestTable[ k ] = v
			elseif aOverwrite then
				aDestTable[ k ] = v
			end
		end
	end
end

function copyTable( aSrcTable, aDestTable, aOverwrite )
	assert( aSrcTable ~= nil and type( aSrcTable ) == "table" )
	-- If no aDestTable then just return a fresh copy of aSrcTable
	aDestTable = aDestTable or {}
	assert( type( aDestTable ) == "table" )
	
	-- Default operation is to overwrite all of aDestTables's keys with aSrcTable's
	-- set to false to only overwrite when not defined
	if aOverWrite == nil then 
		aOverWrite = true
	end

	_copyTable( aSrcTable, aDestTable, aOverwrite )

	return aDestTable
end

-- N.B. Shallow copy (doesn't handle nested tables)
function newTableFromDefaultsOverriddenWithParams( aDefaults, aParams )
	assert( aDefaults and aParams )
	local t = copyTable( aParams, {}, true)  -- Copy aParams so there are no side effects from overwriting values
	return copyTable( aDefaults, t, false )	-- Fill in the blanks from defaults
end

function tableKeyCount( aTable )
	local cnt = 0
	for key, _ in pairs( aTable ) do
		cnt = cnt + 1
	end
	return cnt
end

function tableKeys( aTable )
	local keys = {}
	for key, _ in pairs( aTable ) do
		keys[ #keys + 1 ] = key
	end
	return keys
end

function printTableToFile( aTable, aPath )
	local file = io.open( aPath, "w" )
	assert( file )

	file:write( print_r( aTable, true ) )
	io.close( file )
end

local mt_table = {
	--==== Set ====
	isValueInTable = function( self, aValue )
		for _, v in ipairs( self ) do
			if v == aValue then
				return true
			end
		end
		return false
	end,

	isKeyInTable = function( self, aKey )
		for k, _ in pairs( self ) do
			if k == aKey then
				return true
			end
		end
		return false
	end,

	--==== Stack ====
	push = function( self, aValue )
		self[ #self + 1 ] = aValue
	end,

	pop = function( self )
		return table.remove( self, #self )
	end,

	--==== Queue ====
	popFront = function( self )
		return table.remove( self, 1 )
	end,

	pushFront = function( self, aValue )
		return table.insert( self, 1, aValue )
	end,

	pushBack = function( self, aValue )
		self[ #self + 1 ] = aValue
	end,

	--==== Associative array ====
	keys = function( self )
		return tableKeys( self )
	end,

	keyCount = function( self )
		return tableKeyCount( self )
	end,

	clear = function( self )
		for i, value in ipairs( self ) do
			self[i] = nil
		end
		table.n = 0
	end,

	arrayDeleteIndices = function( self, aIndexList )
		local cnt = 0
		for _, i in ipairs( aIndexList ) do
			table.remove( self, i - cnt )
			cnt = cnt + 1
		end
	end
}

-- Ext
function extendTable( aTable )
	if not aTable[ 'pushBack' ] then
		for funcName, func in pairs( mt_table ) do
			aTable[ funcName ] = func
		end
	end
	return aTable -- chainable
end


--[[
function saveTableToFile( aTable, aFilePath, aPretty )
	-- print( "saveTableToFile" )
	local file = io.open( aFilePath, "w" )
	
	aPretty = aPretty or false
	
	local function _serialise( t, s, d )
		--print( "_serialise" )
		if s == nil then
			s = ""
		end
		
		-- Padding
		d = d or 0
		local padding = ""
		local NL = ""
		if aPretty then
			padding = string.rep( "  ", d )
			NL = "\n"
		end
		
		for k,v in pairs( t ) do
			s = s .. padding
			--#print( k .. ":" .. tostring( v ) )
			if type( v ) == "string" then 
				local _v = ""
				for i=#v, 1, -1 do
					_v = v:byte(i) .. "#" .. _v
				end
				s = s .. "s:" .. k .. "=" .. _v
			elseif type( v ) == "number" then 
				s = s .. "n:" .. k .. "=" .. v
			elseif type( v ) == "table" then 
				s = s .. "t:" .. k .. "=[" .. NL 
				s = _serialise( v, s, d + 1 ) .. padding .. "]"
			elseif type( v ) == "boolean" then 
				if v then 
					s = s .. "b:" .. k .. "=t"
				else
					s = s .. "b:" .. k .. "=f"
				end
			end
			s = s .. "," .. NL
		end
		--print( "_serialise-END" )
		return s
	end
	
	file:write( _serialise( aTable ) )
	io.close( file )
end

function loadTableFromFile( aFilePath )	
	--print( "loadTableFromFile" )
	assert( aFilePath )
	local file = io.open( aFilePath, "r" )	
	
	-- File doesn't exist
	if not file then
		return false
	end

	-- Read file contents into a string
	local dataStr = file:read( "*a" )
	io.close( file ) -- important!

	-- Remove whitespace from input
	dataStr = dataStr:gsub( "[\t\r\n ]", "")
		
	local function _deserialise( aData, aRet, aParentTableStack )
		--print( "_deserialise" )
		--print( aData )
		-- End condition
		if #aData == 0 then
			return
		end
		
		-- Get type
		local varType = aData:sub( 1, 1 )
		
		-- end of a table
		if varType == "]" then 
			aRet = aParentTableStack[ #aParentTableStack ]
			aParentTableStack[ #aParentTableStack ] = nil
			_deserialise( aData:sub( 3 ), aRet, aParentTableStack )  -- Skip "]," therefore idx 3				
		else
			-- Get key 
			local idx,_ = aData:find( "=", 3, true )
			local key = aData:sub( 3, idx - 1 )
			-- Deal with integer indices
			if tonumber( key ) then
				key = tonumber( key )
			end
	
			-- Update data to remainder of the string  TODO %% this is costly can't we improve by just incrementing a pointer into the array?
			local aData = aData:sub( idx + 1 )
			--print( "key: " .. key .. ":" .. aData )
			if varType == "t" then -- start of a table
				aRet[ key ] = {}
				aParentTableStack[ #aParentTableStack + 1 ] = aRet
				_deserialise( aData:sub( 2 ), aRet[ key ], aParentTableStack )	 -- Skip "[" therefore idx 2					
			else
				-- Value
				local _,idx = aData:find( ",", 1, true )
				local value = aData:sub( 1, idx - 1)
						
				if varType == "s" then 
					value = split( value, "#" )
					value = string.char( unpack( value ) )
				elseif varType == "n" then 
					value = tonumber( value )		
				elseif varType == "b" then 
					if value == "t" then
						value = true
					else
						value = false
					end
				end
				--print( varType .. ":" .. key .. "=" .. tostring( value ) )
				aRet[ key ] = value
				_deserialise( aData:sub( idx + 1 ), aRet, aParentTableStack )				
			end
		end			
		--print( "_deserialise--END" )
	end
	
	ret = {}
	 _deserialise( dataStr, ret, { ret } )	
	return ret
end
]]
--===============================================
-- String 
--===============================================
-- See: http://forum.videolan.org/viewtopic.php?f=29&t=90796   - Useful LUA functions
function lastIndexOf(text, search)   -- by ale5000
   local start, last_pos = 0
   while start ~= nil do
      last_pos = start
      start = text:find(search, start + 1, true)
   end

   if last_pos == 0 then return nil end
   return last_pos
end

function urlEncode( aURL )
	if aURL then
		aURL = string.gsub( aURL, "\n", "\r\n")
		--aURL = string.gsub( aURL, "([^%w ])", function( aCode ) 
		aURL = string.gsub( aURL, "([^%w])", function( aCode ) 
			return string.format( "%%%02X", string.byte( aCode ) ) 
		end )
		--aURL = string.gsub( aURL, " ", "+" )
	end
	return aURL	
end

function urlDecode( aURL )
	--aURL = string.gsub( aURL, "+", " " )
	aURL = string.gsub( aURL, "%%(%x%x)", function( aHex ) 
		return string.char( tonumber( aHex, 16 ) ) 
	end )
	aURL = string.gsub( aURL, "\r\n", "\n" )
	return aURL
end

function trim(s)
  return s:match'^()%s*$' and '' or s:match'^%s*(.*%S)'
end

-- str.lua (adds string "split" function, similar to explode() in PHP)
--
-- See more useful Lua snippets at http://lua-users.org/wiki/

function split( str, pat, aTrim )
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local aTrim = aTrim or false
   
   -- Call a trim function when aTrim is true
   local _trim = function( aString ) return aString end
   if aTrim then
   	_trim = trim
   end
   
   local s, e, cap = str:find(fpat, 1)
   while s do
	   -- Trim can reduce to an empty string
	   cap = _trim( cap )
      if s ~= 1 or cap ~= "" then
	 		table.insert(t, cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
	   -- Trim can reduce to an empty string
      cap = _trim( str:sub(last_end) )
      if #cap > 0 then
	      table.insert(t, cap)
	   end
   end
   return t  
end

function splitOnTags( aString )
	local strings, tags = {}, {}
	local start = 1
	local regEx = "([<>]%s*[%a%d%s/]+[%a%d%s/]*[<>])"
	local sPos, ePos, tag = aString:find( regEx, start )
	while sPos do
		local res = aString:sub( start, sPos - 1 )		
		if #res > 0 then
			strings[ #strings + 1 ] = res
		end
		tags[ #tags + 1 ] = tag
		start = ePos + 1
		sPos, ePos, tag = aString:find( regEx, start )
	end
	local res = aString:sub( start )		
	if #res > 0 then
		strings[ #strings + 1 ] = res
	end
	
	return strings, tags
end

--===============================================
-- Array extensions
--===============================================

function shuffleArray( aArray )
	local len = #aArray
	--print( "shuffleArray:" .. len )
	for i=1, len do
		local tempIdx = math.random(1, len)
		-- Perform swap
		aArray[ i ], aArray[ tempIdx ] = aArray[ tempIdx ], aArray[ i ]
	end
end

function concatArray( aSrc, aDst ) 
	for _, v in ipairs( aSrc ) do
		aDst[ #aDst + 1 ] = v
	end
end

-- Doesn't sort the array to speed up comparison because the order of the values in the array may be important (difference between a set and a vector
-- e.g. the array might contain randomised data that sorting would de-randomise
function removeDuplicatesArray( aArray )
	local i=1
	while i <= #aArray do
		local j = i+1
		while j <= #aArray do
			if aArray[j] == aArray[i] then
				table.remove( aArray, j )
			else
				j = j+1
			end
		end
		i = i+1
	end
	--for i=1, #aArray do print( aArray[i] ) end
end

--===============================================
-- Debug
--===============================================
-- forward referenced above
printf = function( aFormat, ... ) 
	print( string.format( aFormat, ... ) )
	--return io.stdout:write( string.format ( ... ))
end

printH1 = function( aMsg )
	printf( "========== %s =========", tostring( aMsg) )
end

dumpDeviceInfo = function()

	--==== App info ====
	print ("               Display Name : ", MOAIEnvironment.appDisplayName)
	print ("                     App ID : ", MOAIEnvironment.appID)
	print ("                App Version : ", MOAIEnvironment.appVersion)

	--==== Screen info  ====	
	print ("               Screen Width : ", MOAIEnvironment.horizontalResolution)
	print ("              Screen Height : ", MOAIEnvironment.verticalResolution)
	print ("                 Screen DPI : ", MOAIEnvironment.screenDpi)
	print ("         iOS Retina Display : ", MOAIEnvironment.iosRetinaDisplay)

	--==== Device info ====
	print ("                       UDID : ", MOAIEnvironment.udid)	
	print ("                  Open UDID : ", MOAIEnvironment.openUdid)	
	print ("                   OS Brand : ", MOAIEnvironment.osBrand)
	print ("                 OS Version : ", MOAIEnvironment.osVersion)
	print ("               Device Model : ", MOAIEnvironment.devModel)

	print ("               Country Code : ", MOAIEnvironment.countryCode)
	print ("              Language Code : ", MOAIEnvironment.languageCode)

	print ("            Device Platform : ", MOAIEnvironment.devPlatform)
	print ("             Device Product : ", MOAIEnvironment.devProduct)
	print ("                    CPU ABI : ", MOAIEnvironment.cpuabi)
	print ("       Number of processors : ", MOAIEnvironment.numProcessors)
	print ("               Device Brand : ", MOAIEnvironment.devBrand)
	print ("                Device Name : ", MOAIEnvironment.devName)
	print ("        Device Manufacturer : ", MOAIEnvironment.devManufacturer)

	--==== Directory info ====
	print ("            Cache Directory : ", MOAIEnvironment.cacheDirectory)
	print ("         Document Directory : ", MOAIEnvironment.documentDirectory)
	print ("         Resource Directory : ", MOAIEnvironment.resourceDirectory)

	--==== Carrier info ====
	print ("   Carrier ISO Country Code : ", MOAIEnvironment.carrierISOCountryCode)
	print ("Carrier Mobile Country Code : ", MOAIEnvironment.carrierMobileCountryCode)
	print ("Carrier Mobile Network Code : ", MOAIEnvironment.carrierMobileNetworkCode)
	print ("               Carrier Name : ", MOAIEnvironment.carrierName)
	print ("            Connection Type : ", MOAIEnvironment.connectionType)
end

-- aFilter = { 'key1', 'key2' }
function print_r( t, aReturnString, aFilter )
	aReturnString = aReturnString or false

	local s = ""
	if type( t ) ~= 'table' then
		s = tostring( t )
	else
		-- Convert { 'key1', 'key2' } => { 'key1' = true, 'key2' = true }
		local filter
		if aFilter then
			filter = {}
			for _, v in ipairs( aFilter ) do
				filter[ v ] = true
			end
		end

		local print_r_cache={}
		local function sub_print_r( tableName, t, indent)
			local ts = tostring(t)
			if ( print_r_cache[ ts ] ) then
				s = string.format( "%s%s[%s] = *%s {},\n", s, indent, tableName, ts:sub( 8 ) )
			else
				print_r_cache[ ts ]=true
				if (type(t)=="table") then
					s = string.format( "%s%s[%s] = %s {\n", s, indent, tableName, ts:sub( 8 ) )
					local indentInner = indent .. string.rep(" ", 3 )
					for key,val in pairs(t) do
						-- s = string.format( "%s%s[%s] => \n", s, indent, tostring( key ) )
						if (type(val)=="table") then
							sub_print_r( key, val, indentInner )
						elseif filter == nil or filter[ key ] or type(key) == 'number' then 
							s = string.format( "%s%s[%s] = '%s',\n", s, indentInner, tostring( key ), tostring( val ) )
						end
					end
					s = string.format( "%s%s}\n", s, indent )
				else
					s = string.format( "%s%s%s\n", s, indent, tostring(t) )
				end
			end
		end

		--==== Start ====	
		sub_print_r( "root table", t," ")
	end

	if aReturnString then
		return s
	else
		print( s )
	end
end
