local Init = require( 'lib-init' ).getSingleton()

local Utils = require( 'lib-utils' )

local MOAIProp2D = MOAIProp2D
local MOAIScriptDeck = MOAIScriptDeck
local MOAITextBox = MOAITextBox
local MOAIFont = MOAIFont
local MOAITextStyle = MOAITextStyle
local MOAIDebugLines = MOAIDebugLines
local MOAILayer2D = MOAILayer2D
local MOAISim = MOAISim
local MOAIViewport = MOAIViewport
local MOAITransform2D = MOAITransform2D
local MOAIGfxDevice = MOAIGfxDevice
local MOAIDraw = MOAIDraw

local print = print
local printf = Utils.printf
local tostring = tostring
local string = string
local assert = assert
local unpack = unpack
local pairs = pairs

local _G = _G

local GFX = {}
setfenv( 1, GFX )

GFX.LAYER_WORLD = 99 -- 0,0 centre, y +ve up
GFX.LAYER_2D = 100   -- 0,0 TL,     y +ve down

--==== Drawing primitives ====
--[[
aRenderCallback = function()
	MOAIGfxDevice
		.setPenColor( 1, 0, 0, 1 ) -- rgba 0-1
		.setPenWidth( 2 )
		.setPointSize( 4 )
	MOAIDraw.
		drawBoxOutline
	 	drawCircle
	 	drawEllipse
	 	drawLine
	 	drawPoints
	 	drawRay
	 	drawRect
	 	fillCircle
	 	fillEllipse
	 	fillFan
	 	fillRect	
end
]]
GFX.newScriptProp = function( aRenderCallback, aLayer )
	local scriptDeck = MOAIScriptDeck.new()
	scriptDeck:setRect ( -1, -1, 1, 1 ) -- Set the model space dimensions of the deck's default rect.
										-- *** Doesn't seem to matter what size you make this rect ?!? ***
	scriptDeck:setDrawCallback( aRenderCallback )
	local prop = MOAIProp2D.new()
	prop:setDeck( scriptDeck )

	if aLayer then 
		aLayer:insertProp( prop )
	end
	return prop, scriptDeck
end

--==== Font ====
local fontDefaults = {
	charcodes = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 .,:;!?()&/-',
	font = 'Arial',  -- built in font for iPhone
	size = 12
}

GFX.newStaticFont = function( aParams )
	local params = Utils.newTableFromDefaultsOverriddenWithParams( fontDefaults, aParams )

	local font = MOAIFont.new()

	--==== Set filename of font for use when loading glyphs ====
	font:load ( params.font )
	
	--==== Loads and caches glyphs for quick aess later ====
		-- TODO: Do we need to specifiy this from MOAIEnvironment.deviceDPI for crisp fonts on Android? or retina devices?
		-- dpi	( number ) Optional. The device DPI (dots per inch of device screen). 
		-- Default value is 72 (points same as pixels).
	font:preloadGlyphs( params.charcodes, params.size )
	return font
end

--==== Style ====
	-- You can attach named styles to a MOAITextbox to be applied to the text using style escapes. 
	-- You can also inline style escapes to control color. Style escapes may be nested.
	-- TODO: Find out how changing the font size effects texture memory
GFX.newStyle = function( aFont, aSize )
	local style = MOAITextStyle.new()
	style:setFont( aFont )
	style:setSize( aSize )
	return style
end

--==== Textbox ====
local textBoxDefaults = {
	width = 100,
	height = 100,
	horizAlign = MOAITextBox.LEFT_JUSTIFY, -- MOAITextBox.[LEFT_JUSTIFY|CENTER_JUSTIFY|RIGHT_JUSTIFY]	
	vertAlign = MOAITextBox.LEFT_JUSTIFY, -- MOAITextBox.[LEFT_JUSTIFY|CENTER_JUSTIFY|RIGHT_JUSTIFY]
}

GFX.newTextBox = function( aText, aParams )
	assert( aText )
	local params = Utils.newTableFromDefaultsOverriddenWithParams( textBoxDefaults, aParams )

	local textbox = MOAITextBox.new()

	textbox:setString( aText )
	
	--==== Set dimensions ====
	textbox:setRect( 0, 0, params.width, params.height )
	textbox:setAlignment( params.horizAlign, params.vertAlign )

	return textbox
end



--==== Viewport/layer ====
-- aType [GFX.LAYER_WORLD|GFX.LAYER_2D]
GFX.newViewport = function( aType )
	assert( aType == GFX.LAYER_2D or aType == GFX.LAYER_WORLD )
	local viewport = MOAIViewport.new()

	local SCREEN_WIDTH, SCREEN_HEIGHT = Init:getDeviceScreenDims()
	local CANVAS_WIDTH, CANVAS_HEIGHT = Init:getCanvasDims()

	if aType == GFX.LAYER_2D then
		-- set size of area on open gl surface to map to 
		viewport:setSize( SCREEN_WIDTH, SCREEN_HEIGHT )   

		-- set 'window' into world space (y axis positive down)
		viewport:setScale( CANVAS_WIDTH, -CANVAS_HEIGHT ) 

		-- Put 0,0 at top, left of virtual canvas 
		--    N.B. actual canvas may be bigger in x or y axis leading to -ve axis values still being visible on screen
		viewport:setOffset( -1, 1 )
	else
		-- set size of area on open gl surface to map to 
		viewport:setSize( SCREEN_WIDTH, SCREEN_HEIGHT )   

		-- set 'window' into world space (y axis positive up)
		viewport:setScale( CANVAS_WIDTH, CANVAS_HEIGHT ) 
	end
	return viewport
end

-- aType [GFX.LAYER_WORLD|GFX.LAYER_2D]
GFX.newLayer = function( aType, aViewport )
	assert( aType == GFX.LAYER_2D or aType == GFX.LAYER_WORLD )
	assert( aViewport )

	local layer = MOAILayer2D.new()
	MOAISim.pushRenderPass( layer )
	layer:setViewport( aViewport )

	-- Position correctly relative to screen which may have a different 
	-- layer:setLoc( Init:getCanvasOffset() )

	layer.type = aType

	return layer
end

--==== Debug lines ====
GFX.debugLinesTextBox = function()
	local red   = { 1.0, 0.0, 0.0, 1.0 }
	local green = { 0.0, 1.0, 0.0, 1.0 }
	local blue  = { 0.0, 0.0, 1.0, 1.0 }
	MOAIDebugLines.setStyle( MOAIDebugLines.TEXT_BOX, 1, unpack( red ) )
	MOAIDebugLines.setStyle( MOAIDebugLines.TEXT_BOX_LAYOUT, 1, unpack( green ) )
	MOAIDebugLines.setStyle( MOAIDebugLines.TEXT_BOX_BASELINES, 1, unpack( blue ) )
end

GFX.debugLinesProp = function( aUseModel )
	if aUseModel then
		MOAIDebugLines.setStyle ( MOAIDebugLines.PROP_MODEL_BOUNDS, 1, 0, 1, 0, 1 )
	else
		MOAIDebugLines.setStyle ( MOAIDebugLines.PROP_WORLD_BOUNDS, 1, 0, 0, 1, 1 )
	end
end

local CROSS_SIZE = 10
local HALF_CROSS_SIZE = CROSS_SIZE * 0.5
local YELLOW = { 1, 1, 0, 1 }
local BLUE = { 0, 0, 1, 1 }
GFX.debugRefPoint = function( aProp, aLayer )
	local refPoints = newScriptProp( function()
			MOAIGfxDevice.setPenWidth( 3 )

			--==== Pivot ====
			local pivX, pivY = aProp:getPiv()
			MOAIGfxDevice.setPenColor( unpack( BLUE ) ) -- rgba 0-1

			local OVERSIZE = 2
			MOAIDraw.drawLine( { -- horiz
				-HALF_CROSS_SIZE + pivX - OVERSIZE, pivY, 
				HALF_CROSS_SIZE + pivX + OVERSIZE, pivY
			} )
			MOAIDraw.drawLine( { -- vert
				pivX, -HALF_CROSS_SIZE + pivY - OVERSIZE, 
				pivX, HALF_CROSS_SIZE + pivY + OVERSIZE
			} )

			--==== Centre ====
			MOAIGfxDevice.setPenColor( unpack( YELLOW ) ) -- rgba 0-1
			MOAIDraw.drawLine( { -HALF_CROSS_SIZE, 0, HALF_CROSS_SIZE, 0 } )
			MOAIDraw.drawLine( { 0, -HALF_CROSS_SIZE, 0, HALF_CROSS_SIZE } )

		end
	)
	refPoints:setAttrLink( MOAITransform2D.INHERIT_TRANSFORM, aProp, MOAIProp2D.TRANSFORM_TRAIT )
	aLayer:insertProp( refPoints )
	return refPoints
end


return GFX