-- print( 'Init.lua included')
local Utils = require( 'lib-utils' )
local Group = require 'lib-group'

local MOAIEnvironment = MOAIEnvironment
local MOAIFileSystem = MOAIFileSystem
local MOAISim = MOAISim
local MOAIViewport = MOAIViewport
local MOAILayer2D = MOAILayer2D

local string = string
local setmetatable = setmetatable
local tostring = tostring
local print = print
local unpack = unpack
local ipairs = ipairs
local pairs = pairs
local require = require
local math = math
local table = table
local assert = assert
local MOAIGfxDevice = MOAIGfxDevice
local printf = Utils.printf

local init = {}
setfenv( 1, init )

--==== Metatable for init object ====
local mt_init = {}

--==== Take defaults from config.lua ====
function mt_init:loadConfigDefaults()

	--==== Include config.lua ====
	if MOAIFileSystem.checkFileExists( 'config.lua' ) then
		local Config = require( "config" )
		local canvas = Config.canvas
		if canvas then
			if canvas.width then
				self.canvasWidth = math.max( 0, math.min( canvas.width, 4000 ) )
			end
			if canvas.height then
				self.canvasHeight = math.max( 0, math.min( canvas.height, 4000 ) )
			end
			if canvas.useImageSizeFromRes then
				self.useImageSizeFromRes = canvas.useImageSizeFromRes
			end
			if canvas.imgResolution then
				self.imgResolution = Utils.copyTable( canvas.imgResolution )
			end
	   end
	   if self.isOSX then
		    local osx = Config.OSX or Config.osx
		    if osx then
		    	if osx.width then
		    		self.screenWidth = math.max( 0, math.min( osx.width, 4000 ) )
		    	end
		    	if osx.height then
		    		self.screenHeight = math.max( 0, math.min( osx.height, 4000 ) )
		    	end
		    	if osx.title then
		    		self.title = osx.title
		    	end
		    end
	    end

	    --==== Cleanup ====
	    Utils.unloadModule( Config )
	    Config = nil
	end
end

function mt_init:setCanvasDims( aCanvasWidth, aCanvasHeight )
	assert( aCanvasWidth and aCanvasHeight )
	printf( "setCanvasDims( %d, %d )", aCanvasWidth, aCanvasHeight )
	self.canvasWidth = aCanvasWidth
	self.canvasHeight = aCanvasHeight

	if self.isOSX then
		self.screenWidth = aCanvasWidth
		self.screenHeight = aCanvasHeight
	end
end

function mt_init:setBackgroundColor( aColor )
	-- if #aColor == 4 then
	-- 	table.remove( aColor, 4 )
	-- end
	MOAIGfxDevice.setClearColor( unpack( aColor ) )
end

function mt_init:initScreen()
	printf( 'initScreen() ')

	--==== OSX ====
	if self.isOSX then 
		-- Required to open a window a computer e.g. OSX host -- CRASHES without this defined
		-- N.B. When using the osx host with the -e command line switch to have auto-restart when 
		-- a source file changes *** changing the window dimensions doesn't have any effect -- you need 
		-- to completely restart ***

		-- You don't seem to need to call this for iOS devices etc
		-- The width seems to be ignored. The height works. 

		MOAISim.openWindow( self.title, self.screenWidth, self.screenHeight )
	end
	print( self.screenWidth, self.screenHeight )
	print( self.canvasWidth, self.canvasHeight )

	--==== Resize canvas to be the same shape as the device screen ====
	local canvasAspectRatio =  self.canvasHeight / self.canvasWidth
	local deviceAspectRatio =  self.screenHeight / self.screenWidth

	local adjustedCanvasWidth, adjustedCanvasHeight = self.canvasWidth, self.canvasHeight
	if deviceAspectRatio < canvasAspectRatio then -- Device is wider than canvas
		adjustedCanvasWidth = self.canvasHeight / deviceAspectRatio
	elseif deviceAspectRatio > canvasAspectRatio then -- Device is taller than canvas
		adjustedCanvasHeight = self.canvasWidth * deviceAspectRatio	
	end
	-- print( adjustedCanvasWidth, adjustedCanvasHeight )
	
	self.canvasOffsetX, self.canvasOffsetY = 0, 0
	self.canvasOffsetX = (adjustedCanvasWidth - self.canvasWidth)*0.5
	self.canvasOffsetY = (adjustedCanvasHeight - self.canvasHeight ) * 0.5
	print( "canvasOffset", self.canvasOffsetX, self.canvasOffsetY )

	self.canvasWidth = adjustedCanvasWidth
	self.canvasHeight = adjustedCanvasHeight
	self.halfCanvasWidth, self.halfCanvasHeight = self.canvasWidth * 0.5, self.canvasHeight * 0.5

	--==== image extension to use (closest to scale) ====
	-- aspect ratio of canvas and device now the same so only need one value
	local scale = self.screenWidth / adjustedCanvasWidth
	local diff = 9999
	for	extension, ratio in pairs( self.imgResolution ) do
		local d = math.abs( scale - ratio )
		if d < diff then
			diff = d
			self.ext = extension
		end
	end
	--print( _ext, scale )
end

function mt_init:getAppInfo()
	return MOAIEnvironment.appDisplayName, MOAIEnvironment.appID, MOAIEnvironment.appVersion
end

--==== Platform ====
function mt_init:isOSX()
	return self.isOSX
end

function mt_init:isAndroid()
	return self.isAndroid
end

function mt_init:isIOS()
	return self.isIOS
end

function mt_init:isRetina()
	return self.isRetina
end

function mt_init:isSimulator()
	return self.isSimulator
end

function mt_init:isIPhone() 
	return self.isIPhone
end

function mt_init:isIPad()
	return self.isIPad
end


--==== Screen/resolution ====
function mt_init:getDeviceScreenDims()
	return 	self.screenWidth, self.screenHeight
end

function mt_init:getCanvasDims()
	return self.canvasWidth, self.canvasHeight	
end

function mt_init:getHalfCanvasDims()
	return self.halfCanvasWidth, self.halfCanvasHeight
end

function mt_init:getCanvasOffset()
	return self.canvasOffsetX, self.canvasOffsetY
end

function mt_init:getExt()
	return self.ext
end

function mt_init:getDefaultExt()
	return self.useImageSizeFromRes
end

--==== Language ====
function mt_init:getLanguageInfo()
	return MOAIEnvironment.countryCode, MOAIEnvironment.languageCode
end

--==== PRIVATE ====
local _new = function()
	local newInit = {
		isOSX = false,
		isAndroid = false,
		isIOS = false,
		isSimulator = false,
		isRetina = false,
		isIPhone = false,
		isIPad = false,

		screenWidth =  MOAIEnvironment.horizontalResolution or 640, -- OSX host: horizontalResolution == nil
		screenHeight = MOAIEnvironment.verticalResolution or 960,
		canvasWidth = 640,
		canvasHeight = 960,
		halfCanvasWidth = 320,
		halfCanvasHeight = 480,
		canvasOffsetX = 0,
		canvasOffsetY = 0,
		backgroundColor = { 1, 1, 1, 1 },

		--==== Use image sizes for packed textures with this file extension ====
		--==== This size should be a match for the canvas size  ====
		useImageSizeFromRes = "@2x", 

		title = 'No title',
		
		imgResolution = {
        	-- canvas/screen ratio <= to this value then use the image extension
            [ "@1x"] = 0.55 ,
            -- [ "@LDPI" ] = 0.75,
            [ "@2x" ] = 1.5,
            [ "@4x" ] = 2.5,
        },		
		
		ext,  		-- resolution of image to use based on screen size
	}
	setmetatable( newInit, { __index = mt_init } )

	--==== initDeviceInfo ====
	local devModel = MOAIEnvironment.devModel or ""

	if devModel:find( 'Simulator' ) ~= nil then
		newInit.isSimulator = true
	end

	local osBrand = MOAIEnvironment.osBrand
	if osBrand then
		if osBrand == 'OSX' then
			newInit.isOSX = true
		elseif osBrand == 'Android' then
			newInit.isAndroid = true
		elseif osBrand == 'iOS' then
			newInit.isIOS = true
		end
	end

	if MOAIEnvironment.iosRetinaDisplay and MOAIEnvironment.iosRetinaDisplay == 'TRUE' then
		newInit.isRetina = true
	end

	devModel = devModel:gsub( ' Simulator', '' )
	if devModel == 'iPhone' then
		newInit.isIPhone = true
	elseif devModel == 'iPad' then
		newInit.isIPad = true
	end

    --==== OSX host: Default to same size as canvas ====
    if newInit.isOSX then
    	newInit.screenWidth =  newInit.canvasWidth
		newInit.screenHeight = newInit.canvasHeight
	end

	return newInit
end 

local _singleton = nil
--==== PUBLIC ====
function getSingleton()
	if _singleton == nil then 
		print( "Creating init")
		_singleton = _new()
	end
	print( _singleton )
	return _singleton
end
--[[
MOAIEnvironment.screenDpi
MOAIEnvironment.udid
MOAIEnvironment.osBrand
MOAIEnvironment.osVersion
]]

--[[
--==== Uncomment to test ====
function True()  print "true"  end
function False() print "false" end
print( 'getAppInfo:',  getAppInfo() )
print( string.format( 'isOSX: %s', tostring( isOSX() ) ) )
print( string.format( 'isAndroid: %s', tostring( isAndroid() ) ) )
print( string.format( 'isIOS: %s', tostring( isIOS() ) ) )
print( string.format( 'isRetina: %s', tostring( isRetina() ) ) )
print( 'getDeviceScreenDims:', getDeviceScreenDims() )
print( 'getLanguageInfo:', getLanguageInfo() )
print( 'isSimulator:', isSimulator() )
print( 'isIPhone:', isIPhone() )
print( 'isIPad:', isIPad() )
--]]
return init