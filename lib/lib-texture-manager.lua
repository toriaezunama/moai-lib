local Init = require( 'lib-init' )

local Utils = require( 'lib-utils' )
local GFX = require( 'lib-gfx' )

local MOAIGfxQuadDeck2D = MOAIGfxQuadDeck2D
local MOAIGfxQuad2D = MOAIGfxQuad2D
local MOAIProp2D = MOAIProp2D
local MOAITexture = MOAITexture
local MOAIFileSystem = MOAIFileSystem
local MOAIImage = MOAIImage

local printf = Utils.printf
local print_r = Utils.print_r
local print = print
local string = string
local tostring = tostring
local assert = assert
local unpack = unpack
local table = table
local ipairs = ipairs
local pairs = pairs
local require = require

local TextureManager = {
	decks = {}
}
setfenv( 1, TextureManager )

--==== Helpers ====
function _getTexturePath( aExt, aTexture )
	return string.format( '_%s/packed/%s.png', aExt, aTexture:gsub( '-packed', '' ) )
end

--==== Deck member functions ====
local _indexForName = function( self, aName )
	--print( 'indexForName', aName )
	assert( self.map[ aName ], string.format( "'%s' not found in %s" , tostring( aName ), tostring( self.texture )) )
	return self.map[ aName ]
end

--==== PRIVATE ====
local function _getImageSizes( aPackedInfo )
	local sizes = {}
	for i, frame in ipairs( aPackedInfo.frames ) do 
		local idx = frame.name:find( '[.@]' )
		local name = frame.name:sub( 1, idx - 1 )
		
		sizes[ name ] = frame.spriteSourceSize -- { width = 128, height = 60 },
	end	
	return sizes
end

-- aExt: [@1x|@2x|@4x]
local function _getImageSizesForPackedTextures( aEXT )
	local dir = '_' .. aEXT .. '/packed/'
	print( '_getImageSizesForPackedTextures()', dir )
	local packedList = MOAIFileSystem.listFiles( dir )
	local packedSizesMap = {}
	for _, file in ipairs( packedList ) do
		if file:match( '.lua' ) then
			--print( file )
			local fileStub = file:sub( 1, -5 )  -- remove '.lua'
			--print( fileStub )
			local packed = require( dir .. fileStub ) 

			packedSizesMap[ fileStub ] = _getImageSizes( packed )
	
			--==== Free memory ==== 
			Utils.unloadModule( packed ) 
			packed = nil			
		end
	end
	return packedSizesMap
end


-- OUTPUT
-- {
-- 	imageCnt
-- 	texture
-- 	images = {
-- 		"cat-head" = {	
-- 			ext = "@2x.png"
-- 			width, height
-- 			uvRect = {
--				v0, v1, u0, u1
-- 			}
-- 		},
--		...
-- 	  }
-- }
local function _packedTextureInfoClean( aPackedInfo )
	--print_r( aPackedInfo )
	local cleaned = {}
	cleaned.imageCnt = table.getn( aPackedInfo.frames )
	cleaned.texture = 'imgs/packed/' .. aPackedInfo.texture

	local images = {}
	for i, frame in ipairs( aPackedInfo.frames ) do 
		local idx = frame.name:find( '[.@]' )
		local name = frame.name:sub( 1, idx - 1 )
		
		local image = {}
		images[ name ] = image

		image.ext = frame.name:sub( idx + 1 )

		--== Create images
		local size = frame.spriteColorRect
		image.width, image.height = (size.x + size.width), ( size.y + size.height)

		image.isRotated90 = frame.textureRotated
		
		local uvs = frame.uvRect
		-- Texture pack uv1 -> uv2 vector is TL -> BR
   		-- BUT MOAI's setUVRect is BL -> TR
		image.uvRect = {
			u0 = uvs.u0,
			v0 = uvs.v1,
			u1 = uvs.u1,
			v1 = uvs.v0,
		}
	end	
	cleaned.images = images
	return cleaned
end

--==== Load all packedtexture.lua files for current resolution ====
function _loadPackedTextureInfo( aExt )
	local path = string.format( '_%s/packed/', aExt )
	local packedList = MOAIFileSystem.listFiles( path )
	local packedTextureInfoMap = {}
	for _, file in ipairs( packedList ) do
		if file:match( '.lua' ) then
			local fileStub = file:sub( 1, -5 )  -- remove '.lua'
				--print( fileStub )
			local packed = require( path .. fileStub ) 
			--local fileStub = fileStub:sub( 1, -4 )  -- remove '@nx'
			packedTextureInfoMap[ fileStub ] = _packedTextureInfoClean( packed )
	
			--==== Free memory ==== 
			Utils.unloadModule( packed ) 
			packed = nil			
		end
	end
	return packedTextureInfoMap
end

-- aTexture: e.g. 'packedTexture'
local function _createDeck( self, aTexture )

	local packedInfo = self.packedInfo[ aTexture ]
	if not packedInfo then 
		return nil
	end

	local texturePath = _getTexturePath( self.imgExt, aTexture )
	--print( texturePath )

	local gfxQuadDeck = MOAIGfxQuadDeck2D.new()
	gfxQuadDeck:setTexture( texturePath )
	
	--print( aTexture )
	--Utils.print_r( self.packedInfo )
	local imageCnt = packedInfo.imageCnt
	gfxQuadDeck:reserve( imageCnt )

	local sizesMap = self.packedSizesMap[ aTexture ]
	--print_r( self.packedSizesMap)

	local map = {}
	local i = 1
	for name, image in pairs( packedInfo.images ) do 
		local size = sizesMap[ name ]
		local hw, hh = size.width*0.5, size.height*0.5
		map[ name ] = i

		gfxQuadDeck:setRect( i, -hw, -hh, hw, hh )

		--printf( "%s: w=%f, h=%f", name, hw, hh )
		local uvRect = image.uvRect
		if image.isRotated90 then
	        -- Sprite data is rotated 90 degrees CW on the texture
			-- Vertex order is clockwise from upper left 
			gfxQuadDeck:setUVQuad( i, 
				uvRect.u1, uvRect.v1,
				uvRect.u1, uvRect.v0,
				uvRect.u0, uvRect.v0,
				uvRect.u0, uvRect.v1
			)
		else
			gfxQuadDeck:setUVRect( i, uvRect.u0, uvRect.v0, uvRect.u1, uvRect.v1 )
		end
		i = i + 1
	end

	gfxQuadDeck.map = map
	gfxQuadDeck.imageCnt = imageCnt
	gfxQuadDeck.texture = aTexture
	gfxQuadDeck.indexForName = _indexForName

	return gfxQuadDeck
end

local _getDimsForName = function( self, aName )
	local info = self.map[ aName ]
	return info.x1, info.y1, info.x2, info.y2
end

--==== PUBLIC ====
function TextureManager:init( aDefaultImgSize, aImgExt )
	printf( 'TextureManager:init(%s, %s )', aDefaultImgSize, tostring( aImgExt ))
	self.defaultImgSize = aDefaultImgSize or '@2x'
	self.imgExt = aImgExt or self.defaultImgSize

	--==== Pre-load default sizes for each packed image based on resolution ====
	self.packedSizesMap = _getImageSizesForPackedTextures( self.defaultImgSize )
	--print_r( self.packedSizesMap )

	--==== Pre-load and process all [_@1x|_@2x|_@4x]/packed-image-name.lua packed texture info ====
	self.packedInfo = _loadPackedTextureInfo( self.imgExt )
	--print_r( self.packedInfo )
end

--==== Find if an image name exists in the loaded packed texture info ====
-- N.B. Must be called after init() has been called
function TextureManager:fileExistsInPackedTextures( aFilename )
	--Utils.print_r( self.packedInfo )

	for textureName, textureInfo in pairs( self.packedInfo ) do
		if textureInfo.images[ aFilename ] then
			return textureName
		end
	end
	return nil
end

function TextureManager:newPackedImage( aName, aTexture, aLayer )
	--print( 'TextureManager:newPackedImage()', tostring( aName ), aTexture, tostring( aLayer ) )
	--print_r( self.decks )
	local deck = self.decks[ aTexture ]
	if not deck then
		deck = _createDeck( self, aTexture )

		--==== Allow us to fail upon not being able to find a texture ====
		if not deck then
			return nil
		end

		self.decks[ aTexture ] = deck
	end

	local prop = MOAIProp2D.new()
	prop:setDeck( deck )

	--==== Allow no img to be specified (defaults to index: 1) ====
	if aName then 
		prop:setIndex( deck:indexForName( aName ) )
	end
	prop.deck = deck

	--==== Flip if y is down ====
	if aLayer then
		aLayer:insertProp( prop )
		if aLayer.type == GFX.LAYER_2D then
			prop:flipY()
		end
	end

	return prop	
end

-- aLayer: if set then add the new prop to this layer AND automatically flipY() if layer.type is GFX.LAYER_2D
function TextureManager:newImage( aPath, aWidth, aHeight, aLayer )
	aPath = string.format( "_%s/%s", self.imgExt, aPath )
	--printf( 'TextureManager: newImage(%s): wxh: %dx%d', aPath, aWidth, aHeight )
	
	local deck = self.decks[ aPath ]

	--==== If deck doesn't exist then create ====	
	if not deck then
		--==== Allow texture loading to fail gracefully so we can search else where ====
			--print( tostring( MOAIFileSystem.checkFileExists( aPath ) ) )
		if not MOAIFileSystem.checkFileExists( aPath ) then
			return nil
		end

		local hw, hh = aWidth*0.5, aHeight*0.5

		deck = MOAIGfxQuad2D.new()
		deck:setTexture( aPath )

		deck:setRect( -hw, -hh, hw, hh )  -- BL -> TR
		deck:setUVRect( 0, 1, 1, 0 )  -- BL -> TR

		self.decks[ aPath ] = deck
	end

	--==== Create prop ====
	local prop = MOAIProp2D.new()
	prop:setDeck( deck )
	prop.deck = deck

	--==== Flip if y is down ====
	if aLayer then
		aLayer:insertProp( prop )
		if aLayer.type == GFX.LAYER_2D then
			-- prop:flipY()
		end
	end

	return prop	
end

function TextureManager:newPackedMemoryImage( aTexture )
	--printf( 'TextureManager: newPackedMemoryImage(%s)', aTexture )

	local img = MOAIImage.new()

	local info = self.packedInfo[ aTexture ]
	assert( info )
	if info then
		local texturePath = _getTexturePath( self.imgExt, aTexture )
		--print( texturePath )
		img:load( texturePath )
		local width, height = img:getSize()

		local map = {}
		for name, image in pairs( info.images ) do 
			local uvRect = image.uvRect
			local i = {
				x1 = width * uvRect.u0,
				y1 = height * uvRect.v1,
				
				isRotated90 = uvRect.isRotated90, -- TODO: we are not using this info yet!
				u0 = uvRect.u0,
				v0 = uvRect.v0,
				u1 = uvRect.u1,
				v1 = uvRect.v1,
			}
			i.x2 = i.x1 + image.width
			i.y2 = i.y1 + image.height
			map[ name ] = i
		end
		img.map = map
	end
	--print_r( img.map )
	--==== Add member functions ====
	img.getDimsForName = _getDimsForName

	return img
end

return TextureManager